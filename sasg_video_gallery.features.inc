<?php
/**
 * @file
 * sasg_video_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sasg_video_gallery_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sasg_video_gallery_image_default_styles() {
  $styles = array();

  // Exported image style: video_embed.
  $styles['video_embed'] = array(
    'label' => 'video embed',
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 320,
        ),
        'weight' => 1,
      ),
      6 => array(
        'name' => 'canvasactions_file2canvas',
        'data' => array(
          'xpos' => 'center',
          'ypos' => 'center',
          'alpha' => 100,
          'scale' => '',
          'path' => 'module://sasg_video_gallery/img/play.png',
        ),
        'weight' => 2,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sasg_video_gallery_node_info() {
  $items = array(
    'sasg_video_gallery' => array(
      'name' => t('Video Gallery'),
      'base' => 'node_content',
      'description' => t('Use the <em>video gallery</em> content type to create a collection of videos.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

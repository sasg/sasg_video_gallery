<?php
/**
 * @file
 * sasg_video_gallery.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function sasg_video_gallery_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_sasg_gallery_video_desc'.
  $field_bases['field_sasg_gallery_video_desc'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_sasg_gallery_video_desc',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_sasg_gallery_videos'.
  $field_bases['field_sasg_gallery_videos'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_sasg_gallery_videos',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'video_embed_field',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'video_embed_field',
  );

  return $field_bases;
}

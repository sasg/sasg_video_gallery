<?php
/**
 * @file
 * sasg_video_gallery.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function sasg_video_gallery_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-sasg_video_gallery-field_sasg_gallery_video_desc'.
  $field_instances['node-sasg_video_gallery-field_sasg_gallery_video_desc'] = array(
    'bundle' => 'sasg_video_gallery',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_sasg_gallery_video_desc',
    'label' => 'Gallery Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'node-sasg_video_gallery-field_sasg_gallery_videos'.
  $field_instances['node-sasg_video_gallery-field_sasg_gallery_videos'] = array(
    'bundle' => 'sasg_video_gallery',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'video_embed_field',
        'settings' => array(
          'description' => 1,
          'description_position' => 'bottom',
          'image_link' => 'source',
          'image_style' => 'video_embed',
        ),
        'type' => 'video_embed_field_thumbnail',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_sasg_gallery_videos',
    'label' => 'Video(s)',
    'required' => 1,
    'settings' => array(
      'allowed_providers' => array(
        'vimeo' => 'vimeo',
        'youtube' => 'youtube',
      ),
      'description_field' => 1,
      'description_length' => 128,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'video_embed_field',
      'settings' => array(),
      'type' => 'video_embed_field_video',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Gallery Description');
  t('Video(s)');

  return $field_instances;
}

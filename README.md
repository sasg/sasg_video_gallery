# SASG Video Gallery

Creates a video gallery content type.

### Drupal Contrib Modules

- [Chaos tools](https://www.drupal.org/project/ctools)
- [Display Suite](https://www.drupal.org/project/ds)
- [Features](https://www.drupal.org/project/features)
- [Imagecache Actions](https://www.drupal.org/project/imagecache_actions)
- [Jquery update](https://www.drupal.org/project/jquery_update)
- [Libraries](https://www.drupal.org/project/libraries)
- [Strongarm](https://www.drupal.org/project/strongarm)
- [System Steam Wrapper](https://www.drupal.org/project/system_stream_wrapper)
- [Video Embed Field](https://www.drupal.org/project/video_embed_field)

### Libraries

- [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/)

### Additional Configuration

- For now, Enable Field Templates in Display Suite is manually activated. Go to admin/structure/ds/list/extras, check Enable Field Templates and Save configuration.

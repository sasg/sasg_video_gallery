(function ($) {
	$(document).ready(function() {
		$('.video-gallery a').magnificPopup({
			disableOn: 526,
			type: 'iframe',
			iframe: {
	            patterns: {
	                youtube_short: {
	                  index: 'youtu.be/',
	                  id: 'youtu.be/',
	                  src: '//www.youtube.com/embed/%id%?autoplay=1'
	                }
	            }
	        },
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: false
		});
	});
}(jQuery));
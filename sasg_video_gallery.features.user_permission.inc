<?php
/**
 * @file
 * sasg_video_gallery.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function sasg_video_gallery_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create sasg_video_gallery content'.
  $permissions['create sasg_video_gallery content'] = array(
    'name' => 'create sasg_video_gallery content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any sasg_video_gallery content'.
  $permissions['delete any sasg_video_gallery content'] = array(
    'name' => 'delete any sasg_video_gallery content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own sasg_video_gallery content'.
  $permissions['delete own sasg_video_gallery content'] = array(
    'name' => 'delete own sasg_video_gallery content',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any sasg_video_gallery content'.
  $permissions['edit any sasg_video_gallery content'] = array(
    'name' => 'edit any sasg_video_gallery content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own sasg_video_gallery content'.
  $permissions['edit own sasg_video_gallery content'] = array(
    'name' => 'edit own sasg_video_gallery content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'node',
  );

  return $permissions;
}
